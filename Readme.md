### INSTALLATION
1. Checkout the repository into a folder
2. From the root directory of this folder run **npm install**

### HOW TO RUN 
1. From the root directory of this folder execute **npm run start**
2. Output is generated to a file called **output.html** available in the output folder. Please open in a browser.

### IMPORTANT NOTES
We generate our output from the date our customers have started signing up. It seems our orders file contains a lot more historical information.

### RECOMMENDED IMPROVEMENTS (time permiting) 
1. Unit tests
2. Typescript
3. Input validation for all of our functions