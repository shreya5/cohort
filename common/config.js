module.exports = {
    'cohortSizeInDays': 7,
    'customerFilePath': `${__dirname}/../input/customers.csv`,
    'ordersFilePath': `${__dirname}/../input/orders.csv`,
    'outputFilePath': `${__dirname}/../output/output.html`
};
