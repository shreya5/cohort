const fs = require('fs');
const Papa = require('papaparse');

exports.parseCSVFile = filePath => {

    const fileContents = fs.readFileSync(filePath, 'utf-8');

    return new Promise((resolve, reject) => {

        // parse the CSV file
        Papa.parse(fileContents, {
            'complete' (results) {
                resolve(results.data);
            },
            'header': true
        });

    });

};
