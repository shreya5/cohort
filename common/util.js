const _ = require('lodash');
const moment = require('moment');
const CONFIG = require('./config');
const fs = require('fs');

exports.sortByDate = (listOfObjects, propertyName) => {

    let sortedList = _.sortBy(listOfObjects, item => new moment(item[propertyName]));

    return sortedList;

};

const generateDocumentHeader = () => `<!DOCTYPE html>
      <html>
        <head> 
            <title>Cohort Analysis - Shreya Vakil</title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
            <style>
                table td, table th {
                    min-width: 180px;
                }
            </style>
        </head> 
    `;


const generateFirstRowWithBuckets = cohortContainer => {

    let thead = `<thead class="thead-inverse">
                        <tr>
                            <th> Cohort </th>
                            <th> Customers </th>          
                            `;

    let numberOfBuckets = cohortContainer.numberOfBuckets;
    let startOfBucket = 0;
    let endOfBucket = startOfBucket + CONFIG.cohortSizeInDays - 1;

    for (let i = 0; i < numberOfBuckets; i++) {
        let rangeString = `${startOfBucket} - ${endOfBucket}`;

        thead += `<th> ${rangeString} days </th>`;
        startOfBucket = endOfBucket + 1;
        endOfBucket = startOfBucket + CONFIG.cohortSizeInDays - 1;
    }

    thead += '</tr> </thead>';

    return thead;
};

const generateRowsForEachCohort = cohortContainer => {
    let tbody = '<tbody>';

    cohortContainer.cohortList.forEach(cohort => {

        if (cohort.totalCustomers <= 0) {
            return;
        }

        let eachRow = '<tr>';

        eachRow += `<td>${cohort.startDate} to ${cohort.endDate}</td>`;
        eachRow += `<td>${cohort.totalCustomers}</td>`;

        let totalOrders = cohort.totalOrders;

        cohort.bucketList.forEach(bucket => {
            let percentTotalOrders = (bucket.totalOrders * 100 / totalOrders).toFixed(1);
            let percentFirstTimeOrders = (bucket.firstTimeOrders * 100 / totalOrders).toFixed(1);
            let outputString = `${percentTotalOrders}% orderers (${bucket.totalOrders})<br/>`;

            outputString += `${percentFirstTimeOrders}% 1st time (${bucket.firstTimeOrders})`;
            eachRow += `<td>${outputString}</td>`;
        });

        eachRow += '</tr>';

        tbody += eachRow;

    });

    tbody += '</tbody>';

    return tbody;
};

exports.generateReportAndPrintToFile = cohortContainer => {
    let documentHeader = generateDocumentHeader();
    let documentBody = '<body>';
    let table = '<table class="table table-bordered table-striped">';

    table += generateFirstRowWithBuckets(cohortContainer);
    table += generateRowsForEachCohort(cohortContainer);
    table += '</table>';

    documentBody += table;
    documentBody += '</body>';
    let documentEnd = '</html>';

    let finalHTML = documentHeader + documentBody + documentEnd;
    
    fs.writeFile(CONFIG.outputFilePath, finalHTML, err => {
        if (err) {
            throw err;
        }
    });
};
