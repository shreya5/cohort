
const _ = require('lodash');
const CONFIG = require('./common/config');
const fileOps = require('./common/fileops');
const util = require('./common/util');
const CohortsContainer = require('./models/CohortsContainer');

const ordersFilePromise = fileOps.parseCSVFile(CONFIG.ordersFilePath);
const customersFilePromise = fileOps.parseCSVFile(CONFIG.customerFilePath);
let cohortsContainer;

Promise.all([ordersFilePromise, customersFilePromise])
    .then(data => {
        let orders = data[0];
        let customers = data[1];

        let ordersSortedInAscendingOrder = util.sortByDate(orders, 'created');
        let customersSortedInAscendingOrder = util.sortByDate(customers, 'created');

        // we are receving a redundant blank object as the last item in the array due to sorting using lodash 
        // so let's remove it
        ordersSortedInAscendingOrder.pop();
        customersSortedInAscendingOrder.pop();

        let dateOfFirstCustomerSignup = _.first(customersSortedInAscendingOrder)['created']; 
        let earliestOrderDate = _.first(ordersSortedInAscendingOrder)['created'];
        let mostRecentOrderDate = _.last(ordersSortedInAscendingOrder)['created'];

        cohortsContainer = new CohortsContainer(CONFIG.cohortSizeInDays, dateOfFirstCustomerSignup, mostRecentOrderDate);
        cohortsContainer.allocateCustomersToCohorts(customersSortedInAscendingOrder);
        cohortsContainer.allocateOrdersToCohorts(ordersSortedInAscendingOrder);
        util.generateReportAndPrintToFile(cohortsContainer);

    })
    .catch(err => {
        throw err;
    });

