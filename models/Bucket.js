
class Bucket {

    constructor (startDateAsNumber, endDateAsNumber) {
        this._startDateAsNumber = startDateAsNumber;
        this._endDateAsNumber = endDateAsNumber;
        this._firstTimeOrders = 0;
        this._totalOrderInThisBucket = 0;
    }

    checkAndAssignOrder (orderCreationDateInInteger, hasPreviouslyOrdered) {
        if (this._startDateAsNumber <= orderCreationDateInInteger && orderCreationDateInInteger <= this._endDateAsNumber) {
            this._totalOrderInThisBucket++;
          
            if (!hasPreviouslyOrdered) {
                this._firstTimeOrders++;
            }
            
            return true;
        } 
        
        return false;
        
    }

    get firstTimeOrders () {
        return this._firstTimeOrders;
    }

    get totalOrders () {
        return this._totalOrderInThisBucket;
    }
}

module.exports = Bucket;
