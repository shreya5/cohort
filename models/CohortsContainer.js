const moment = require('moment');
const SingleCohort = require('./SingleCohort');

class CohortsContainer {

    constructor (cohortSizeInDays, earliestDate, lastDate) {
        this._cohortList = [];

        let startDate = moment(earliestDate).startOf('day')
            .valueOf();
        let cutOffDate = moment(startDate).add(cohortSizeInDays, 'days')
            .endOf('day')
            .valueOf();
        let finalDate = moment(lastDate).endOf('day')
            .valueOf();

        let i = 0;

        while (cutOffDate <= finalDate) {
            this._cohortList[i++] = new SingleCohort(startDate, cutOffDate, finalDate);
            startDate = moment(cutOffDate).add(1, 'day')
                .startOf('day')
                .valueOf();
            cutOffDate = moment(startDate).add(cohortSizeInDays, 'days')
                .endOf('day')
                .valueOf();
        }
    }

    get numberOfBuckets () {
        let firstCohort = this._cohortList[0];

        
        return firstCohort.numberOfBuckets;
    }

    get cohortList () {
        return this._cohortList;
    }

    allocateCustomersToCohorts (sortedCustomerList) {
        sortedCustomerList.forEach(customer => {
            let cohortFoundForThisCustomer = false;

            customer.customerJoinDateAsInteger = moment(customer.created).valueOf();
            let i = 0;

            while (!cohortFoundForThisCustomer && i < this._cohortList.length) {
                let currentCohort = this._cohortList[i++];

                cohortFoundForThisCustomer = currentCohort.checkAndAssignCustomer(customer);
            }

        });

    }

    allocateOrdersToCohorts (sortedOrdersList) {
        sortedOrdersList.forEach(order => {
            let cohortFoundForThisOrder = false;

            let i = 0;

            while (!cohortFoundForThisOrder && i < this._cohortList.length) {
                let currentCohort = this._cohortList[i++];

                cohortFoundForThisOrder = currentCohort.checkAndAssignOrder(order);
            }
        });
    }

    printCustomerCountPerCohort () {
        this._cohortList.forEach(cohort => {
            cohort.printMyCustomerCount();
        });
    }

    printCohortRanges () {
        this._cohortList.forEach(cohort => {
            cohort.printRange();
        });
    }

}

module.exports = CohortsContainer;
