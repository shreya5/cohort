const moment = require('moment');
const CONFIG = require('../common/config');
const Bucket = require('./Bucket');

class SingleCohort {

    constructor (startDateAsNumber, endDateAsNumber, finalDateAsNumber) {
        this._startDateAsNumber = startDateAsNumber;
        this._endDateAsNumber = endDateAsNumber;
        this._finalDateAsNumber = finalDateAsNumber;
        this._myCustomers = {};
        this._hasPreviouslyOrderered = {};
        this._totalOrdersInThisCohort = 0;
        this._buckets = [];
        this.initializeDateBuckets();
    }

    get startDate () {
        let temp = this._startDateAsNumber;

        
        return moment(temp).format('YYYY-MM-DD'); 
    }

    get endDate () {
        let temp = this._endDateAsNumber;

        
        return moment(temp).format('YYYY-MM-DD'); 
    }

    get totalCustomers () {
        return Object.keys(this._myCustomers).length;
    }

    get totalOrders () {
        return this._totalOrdersInThisCohort;
    }

    get bucketList () {
        return this._buckets;
    }

    get numberOfBuckets () {
        return this._buckets.length;
    }

    initializeDateBuckets () {
        let startOfRange = this._startDateAsNumber;
        let endOfRange = this._endDateAsNumber;

        let i = 0;

        while (endOfRange <= this._finalDateAsNumber) {
            this._buckets[i++] = new Bucket(startOfRange, endOfRange);
            startOfRange = moment(endOfRange).add(1, 'days')
                .startOf('day')
                .valueOf();
            endOfRange = moment(startOfRange).add(CONFIG.cohortSizeInDays, 'days')
                .endOf('day')
                .valueOf();
        }
    }

    checkAndAssignCustomer (customer) {
        if (customer.customerJoinDateAsInteger >= this._startDateAsNumber && customer.customerJoinDateAsInteger <= this._endDateAsNumber) {
            this._myCustomers[customer.id] = true;
            
            return true;
        } 
        
        return false;
        
    }

    checkAndAssignOrder (order) {
        let isCustomerInThisCohort = this._myCustomers[order.user_id];

        if (isCustomerInThisCohort) {
            this._totalOrdersInThisCohort++;
            let hasPreviouslyOrdered = Boolean(this._hasPreviouslyOrderered[order.user_id]);

            if (!hasPreviouslyOrdered) {
                this._hasPreviouslyOrderered[order.user_id] = true;
            }

            this.allocateOrderToBucket(order, hasPreviouslyOrdered);

            return true;
        } 
        
        return false;
        
    }

    allocateOrderToBucket (order, hasPreviouslyOrdered) {

        let orderCreationDateInInteger = moment(order.created).valueOf();

        order.orderCreationDateInInteger = orderCreationDateInInteger;

        let i = 0;
        let bucketFoundForThisOrder = false;

        while (!bucketFoundForThisOrder && i < this._buckets.length) {
            bucketFoundForThisOrder =
                this._buckets[i++].checkAndAssignOrder(orderCreationDateInInteger, hasPreviouslyOrdered);
        }

    }

}

module.exports = SingleCohort;
